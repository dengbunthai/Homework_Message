package com.example.demo.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repository.MessageRepository;
import com.example.demo.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService{

	MessageRepository messageRepository;
	
	@Autowired
	public MessageServiceImpl(MessageRepository messageRepository) {
		super();
		this.messageRepository = messageRepository;
	}

	@Override
	public Collection<com.example.demo.model.Message> Messages() {
		return messageRepository.Messages();
	}

	@Override
	public com.example.demo.model.Message Message(int id) {
		return messageRepository.Message(id);
	}

}
