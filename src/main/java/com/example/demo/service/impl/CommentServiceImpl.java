package com.example.demo.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repository.CommentRepository;
import com.example.demo.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService{

	CommentRepository commentRepository;
	
	@Autowired
	public CommentServiceImpl(CommentRepository commentRepository) {
		super();
		this.commentRepository = commentRepository;
	}

	@Override
	public Collection<com.example.demo.model.Comment> Comments() {
		return commentRepository.Comments();
	}

	@Override
	public com.example.demo.model.Comment Comment(int id) {
		com.example.demo.model.Comment comment = Comments().stream().filter(c -> c.getId() == id).findFirst().get();
		return comment;
	}

}
