package com.example.demo.service;

import java.util.Collection;

import com.example.demo.model.Comment;

public interface CommentService {
	public Collection<Comment> Comments();
	public Comment Comment(int id);
	
}
