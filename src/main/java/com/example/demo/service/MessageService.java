package com.example.demo.service;

import java.util.Collection;

public interface MessageService {
	public Collection<com.example.demo.model.Message> Messages();
	public com.example.demo.model.Message Message(int id);
}
