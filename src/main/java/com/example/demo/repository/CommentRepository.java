package com.example.demo.repository;

import java.util.Collection;

import com.example.demo.model.Comment;

public interface CommentRepository {

	public Collection<Comment> Comments();
	public Comment Comment(int id);
	
}
