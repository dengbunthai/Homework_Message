package com.example.demo.repository;

import java.util.Collection;



public interface MessageRepository {
	
	public Collection<com.example.demo.model.Message> Messages();
	public com.example.demo.model.Message Message(int id);
	
}
