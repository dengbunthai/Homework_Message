package com.example.demo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Comment;
import com.example.demo.repository.CommentRepository;

@Repository
public class CommentRepositoryImpl implements CommentRepository{

	List<com.example.demo.model.Comment> comments = new ArrayList<com.example.demo.model.Comment>();
	
	public CommentRepositoryImpl(){
		comments.add(new Comment(1, "Hi Dear!!"));
		comments.add(new Comment(2, "How are you!!"));
		comments.add(new Comment(3, "I'm Fine :)"));
		comments.add(new Comment(4, "Why??"));
		comments.add(new Comment(5, "Because I'm hungry"));
		comments.add(new Comment(6, "Okay, I'll give you some foods :D"));		
	}
	
	@Override
	public Collection<com.example.demo.model.Comment> Comments() {
		return comments;
	}

	@Override
	public com.example.demo.model.Comment Comment(int id) {
		Comment comment = Comments().stream().filter(c -> c.getId() == id).findFirst().get();
		return comment;
	}

}
