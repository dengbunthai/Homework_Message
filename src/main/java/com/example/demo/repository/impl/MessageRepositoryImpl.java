package com.example.demo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.repository.MessageRepository;
import com.example.demo.model.Message;
import com.example.demo.model.Comment;

@Repository
public class MessageRepositoryImpl implements MessageRepository{

	@Override
	public Collection<com.example.demo.model.Message> Messages() {
		List<Message> messages = new ArrayList<>();
		Message message1 = new Message();
			message1.setId(1);
			message1.setText("Hello EveryOne");
				List<Comment> comments1 = new ArrayList<>();
					comments1.add(new Comment(1, "Hi Dear!!"));
					comments1.add(new Comment(2, "How are you!!"));
					comments1.add(new Comment(3, "I'm Fine :)"));
			message1.setComments(comments1);
			
		Message message2 = new Message();
			message2.setId(2);
			message2.setText("Today Not feel good");
				List<Comment> comments2 = new ArrayList<>();
				comments2.add(new Comment(4, "Why??"));
				comments2.add(new Comment(5, "Because I'm hungry"));
				comments2.add(new Comment(6, "Okay, I'll give you some foods :D"));
			message2.setComments(comments2);
		
		Message message3 = new Message();
			message3.setId(3);
			message3.setText("Today is quite!!");
			
		messages.add(message1);
		messages.add(message2);
		messages.add(message3);
		
		return messages;
	}

	@Override
	public com.example.demo.model.Message Message(int id) {
		Message message = Messages().stream().filter(m -> m.getId() == id).findFirst().get();
		return message;
	}

}

