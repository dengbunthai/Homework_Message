package com.example.demo.RestController;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Message;
import com.example.demo.service.MessageService;

@RestController
@RequestMapping("api/v1/messages")
public class MessageRestController {

	MessageService messageService;
	
	@Autowired
	public MessageRestController(MessageService messageService) {
		super();
		this.messageService = messageService;
	}

	@GetMapping
	public Collection<Resource<Message>> Messages(){
		Collection<Message> messages = messageService.Messages();
		Collection<Resource<Message>> resources = new ArrayList<Resource<Message>>();
		for(Message message : messages){
			Resource<Message> resource = new Resource<Message>(message);
			Link link = linkTo(methodOn(MessageRestController.class).Message(message.getId())).withSelfRel();
			resource.add(link);

			link = linkTo(methodOn(CommentRestController.class).Comments()).withRel("Cmments");
			resource.add(link);			
			
			resources.add(resource);
		}
		return resources;
	}
	
	@GetMapping("{id}")
	public Resource<Message> Message(@PathVariable int id){
		Message message = messageService.Messages().stream().filter(m -> m.getId() == id).findFirst().get();
		Resource<Message> resource = new Resource<Message>(message);
		Link link = ControllerLinkBuilder.linkTo(methodOn(MessageRestController.class).Message(id)).withSelfRel();
		resource.add(link);
		return resource;
	}
	
}
