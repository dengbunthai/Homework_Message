package com.example.demo.RestController;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Comment;
import com.example.demo.service.CommentService;

@RestController
@RequestMapping("api/v1/comments")
public class CommentRestController {

	CommentService commentService;

	@Autowired
	public CommentRestController(CommentService commentService) {
		super();
		this.commentService = commentService;
	}
	
	@GetMapping
	public Collection<Resource<Comment>> Comments(){
		Collection<Comment> comments = commentService.Comments();
		Collection<Resource<Comment>> resources = new ArrayList<>();
		for(Comment comment : comments){
			Resource<Comment> resource = new Resource<Comment>(comment);
			Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CommentRestController.class).Comment(comment.getId())).withSelfRel();
			resource.add(link);
			resources.add(resource);
		}
		
		return resources;
		
	}
	
	@GetMapping("{id}")
	public Resource<Comment> Comment(@PathVariable int id){
		Resource<Comment> resource = new Resource <Comment>(commentService.Comment(id));
		Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(CommentRestController.class).Comment(id)).withSelfRel();
		resource.add(link);
		return resource;
	}
	
}
